/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.totsp.crossword.shortyz;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.totsp.crossword.shortyz";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 40401;
  public static final String VERSION_NAME = "4.4.1";
}
